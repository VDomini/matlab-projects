% order of unknowns is [xe ye ze vxe vye vze xm ym zm vxm vym vzm]

% 1 year in terms of seconds
T=365.25*24*60*60;
% AU conversion
AU=1.49598e11;


EarthX =   [-27115219762.4,132888652547.0,57651255508.0];  %Earth Pos (x,y,z) meters
EarthV =   [-29794.2199907,-4924.33333333,-2135.59540741]; %Earth Vel (x,y,z) m/s
MoonX  =   [-27083318944,133232649728, 57770257344];       %Moon Pos (x,y,z) meters
MoonV  =   [-30864.2207031,-4835.03349304,-2042.89546204]; %Moon Vel (x,y,z) m/s

EarthAU  = (EarthX/(AU));
EarthVAU = (EarthV/(AU)*T);
MoonAU   = (MoonX/(AU));
MoonVAU  = (MoonV/(AU)*T); 
   
INITIAL= [EarthAU EarthVAU MoonAU MoonVAU];   
    
      
% set ODE
opts=odeset('OutputFcn',@nondimensionalDetect,'RelTol',1e-12,'AbsTol',1e-12);

% use ode45 to solve differential equation given in ForcesNonDimensional
[t,y] = ode45(@ForcesNonDimensional,[0,3],INITIAL,opts);

% plot earth
figure(2)
plot3(y(:,1), y(:,2), y(:,3),'bo-');
grid;
title('Fucking Earth')

%plot moon
figure(3)
plot3(y(:,7), y(:,8), y(:,9), 'blacko-');
grid;
title('Fucking Moon')
figure(gcf);


