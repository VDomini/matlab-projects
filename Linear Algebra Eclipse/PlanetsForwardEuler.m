G = 6.673e-11;
Me = 5.98e24;
Mm = 7.32e22;
Ms = 1.9891e30;

c1 = zeros(1,20);

MP = zeros(3,2);

Ei = [-27115219762.4, 132888652547.0, 57651255508.0];
Mi = [-27083318944, 133232649728, 57770257344];
Si = [0, 0, 0];

timeStep=5e1; %timestep, in number of seconds
numyears=3; %number of years in years


T = numyears*365.24*3600*24;

Ev = [-29794.2199907, -4924.33333333, -2135.59540741];
Mv = [-30864.2207031, -4835.03349304, -2042.89546204];
x = 0;
lastT = 1;
start = datenum(1999, 1, 1, 0, 0, 0); 

for k = 1:timeStep:T
       
        Rem = Ei- Mi;
        
        remm = norm(Rem);
        rmss = norm(Mi);
        rsee = norm(Ei);
        dvxe = -(G*Ms*Ei(1))/(rsee^3) - (G*Mm*(Ei(1)-Mi(1)))/(remm^3);
        dvye = -(G*Ms*Ei(2))/(rsee^3) - (G*Mm*(Ei(2)-Mi(2)))/(remm^3);
        dvze = -(G*Ms*Ei(3))/(rsee^3) - (G*Mm*(Ei(3)-Mi(3)))/(remm^3);
    
        dvxm = -(G*Ms*Mi(1))/(rmss^3) + (G*Me*(Ei(1)-Mi(1)))/(remm^3);
        dvym = -(G*Ms*Mi(2))/(rmss^3) + (G*Me*(Ei(2)-Mi(2)))/(remm^3);
        dvzm = -(G*Ms*Mi(3))/(rmss^3) + (G*Me*(Ei(3)-Mi(3)))/(remm^3);
   
    
        Ei = Ei + Ev*timeStep;
        Mi = Mi + Mv*timeStep;   
        
        Ev = Ev + [dvxe, dvye, dvze]*timeStep;
        Mv = Mv + [dvxm, dvym, dvzm]*timeStep;
    
    
    
    V = dot(Ei, Mi);
    
    dp = dot(-Mi,Ei- Mi);
    cosa = dp/(rmss*remm);
    angle = acos(cosa);
    
    if (angle>= .994*pi)
            c1(1,x+1) = k;
            x = x + 1;
    end
        if (angle< .990*pi)
            if x > 0
            a = round(x/2);
            disp(datestr(start + c1(1,a)/(60*60*24)));
            x = 0;
            c1 = zeros(1,20);
            end
        end
   
    if  k - lastT >24*60*60
        lastT=k;
        subplot(2, 1, 1);
        hold on
        plot3(Ei(1), Ei(2), Ei(3), 'r.-');
        view(0, 65)
        grid on
    
        subplot(2, 1, 2)
        hold on
        plot3(Mi(1)-Ei(1), Mi(2)-Ei(2), Mi(3)-Ei(3), 'b.-');
        grid on
    
        pause(0.0001);
        gcf;
    end

   
end

