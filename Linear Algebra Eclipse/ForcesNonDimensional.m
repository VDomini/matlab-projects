function a = ForcesNonDimensional(t,y)

% 1 year in terms of seconds
T=365.25*24*60*60;

%AU conversion
AU=1.49598e11;

%gravitational constant
G  = 6.673e-11;        

%New gravitational constant, factoring in T and AU
G=G*T^2/AU^3;

%sun/earth/moon/jupiter/uranus/saturn/neptune/venus/mars system


Me = 5.98e24;           %mass earth
Mm = 7.32e22;           %mass moon
Ms = 1.9891e30;         %mass sun


r  = sqrt(y(1)^2 + y(2)^2 + y(3)^2);                      %radius earth to sun
r2 = sqrt(y(7)^2 + y(8)^2 + y(9)^2);                      %radius moon to sun
r3 = sqrt((y(7)-y(1))^2 + (y(8)-y(2))^2 + (y(9)-y(3))^2); %radius moon to earth
%radius earth to sun

a = [
    

      y(4);
      y(5);
      y(6);
      (-G*Ms*y(1))/r^3 - (-G*Mm*(y(7)-y(1)))/r3^3 ;
      (-G*Ms*y(2))/r^3 - (-G*Mm*(y(8)-y(2)))/r3^3 ;
      (-G*Ms*y(3))/r^3 - (-G*Mm*(y(9)-y(3)))/r3^3 ;
      y(10);
      y(11);
      y(12);
      (-G*Ms*y(7))/r2^3 + (-G*Me*(y(7)-y(1)))/r3^3 ; 
      (-G*Ms*y(8))/r2^3 + (-G*Me*(y(8)-y(2)))/r3^3 ; 
      (-G*Ms*y(9))/r2^3 + (-G*Me*(y(9)-y(3)))/r3^3 ; 
      
    ];