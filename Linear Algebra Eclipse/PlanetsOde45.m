% order of unknowns is [xe ye ze vxe vye vze xm ym zm vxm vym vzm]

% 1 year in terms of seconds
T=365.25*24*60*60;

% set ODE
opts=odeset('OutputFcn',@detecteclipse,'RelTol',1e-12,'AbsTol',1e-12);

% use ode45 to solve differential equation given in pracode
[t,y] = ode45(@FORCES,[0,3*T],[-27115219762.4,132888652547.0,57651255508.0,-29794.2199907,-4924.33333333,-2135.59540741,-27083318944,133232649728,57770257344,-30864.2207031,-4835.03349304,-2042.89546204],opts);

% plot earth
figure(2)
plot3(y(:,1), y(:,2), y(:,3),'bo-');
grid;
title('Fucking Earth')

%plot moon
figure(3)
plot3(y(:,7), y(:,8), y(:,9), 'blacko-');
grid;
title('Fucking Moon')
figure(gcf);


