This Repository holds old projects done in MATLAB, which were created in 2012-2013.

The Accelerometer Game was a game written from scratch using a 3D accelerometer to control a circle and dodge objects.

The Linear Algebra Eclipse project was written to predict Eclipses based on mathematics done through linear algebra using Matlab as a calculation platform.