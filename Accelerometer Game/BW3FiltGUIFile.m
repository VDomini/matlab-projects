% NAME: Rod Aluise and James McReynolds
% SECTION: Section 068
% GROUP: Group 5

function varargout = BW3FiltGUIFile(varargin)


% Last Modified by GUIDE v2.5 15-Feb-2013 03:13:24

% Begin initialization code 
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @BW3FiltGUIFile_OpeningFcn, ...
                   'gui_OutputFcn',  @BW3FiltGUIFile_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code 


% --- Executes just before GUI is made visible.
function BW3FiltGUIFile_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to BW3FiltGUIFile (see VARARGIN)

% Choose default command line output for BW3FiltGUIFile
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = BW3FiltGUIFile_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in startSerialButton.
function startSerialButton_Callback(hObject, eventdata, handles)
% hObject    handle to startSerialButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Going to separate this button into two commands. One will Setup the
% Serial Port and Calibrate the accelerometer. The other will Close the
% Serial Port.

% Gets the current text of the button
handles.str = get(handles.startSerialButton,'String');

% This says if it is the original text it will:
if strcmp(handles.str,'Setup and Calibrate')
    
    %Find the COM Port
    handles.comPort = 'COM3';

    %connect MATLAB to the accelerometer
    if (~exist('handles.serialFlag','var'))
        [handles.accelerometer.s,handles.serialFlag] = setupSerial(handles.comPort);
    end
    
    % Change the names of the button and the adjacent text box
    set(handles.startSerialButton,'String','Close Serial');
    set(handles.startSerialButton,'BackgroundColor','red');
    set(handles.serialStatusLabel,'String','On!')
    
    % Calibrate the accelerometer
    if(~exist('handles.calCo', 'var'))
        handles.calCo = calibrate(handles.accelerometer.s);
    end
    
    % Update the GUI
    guidata(hObject, handles);
    
% If the text has been changed (By pressing the button previously then:
else
   %Close serial and end interface.
   closeSerial
end


% --- Executes on button press in doSomethingButton.
function doSomethingButton_Callback(hObject, eventdata, handles)
% hObject    handle to doSomethingButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Stores text on button to a string.
handles.str = get(handles.doSomethingButton,'String');


% If statement to determine what text is currently on button
if strcmp(handles.str,'Start Plot')
    
    % This will start the plot, and switch the button background and text
   set(handles.doSomethingButton,'String','Stop Plot');
   set(handles.doSomethingButton,'BackgroundColor','red');
   set(handles.pushbutton7,'String','Start Plot with Filtering'); 
   set(handles.pushbutton7,'BackgroundColor','green');
   
   % Changes the graph indicator to on
   set(handles.graphIndic,'BackgroundColor','cyan');
   set(handles.graphIndic,'String','Graphs are Active!');
   
   % Updates GUI
    guidata(hObject, handles);
    
else
    
   % This is if the plot is running. Stops plot and switches back to
   % original state
   set(handles.doSomethingButton,'String','Start Plot'); 
   set(handles.doSomethingButton,'BackgroundColor','green');
   set(handles.graphIndic,'String','Graphs are Inactive');
   set(handles.graphIndic,'BackgroundColor','red');
   guidata(hObject, handles);
   
end

%Updates str to enter while loop with correct text
handles.str = get(handles.doSomethingButton,'String');

% Sets up variables for collecting data
handles.buf_len = 200;
handles.gxdata = zeros(handles.buf_len,1);
handles.gydata = zeros(handles.buf_len,1);
handles.gzdata = zeros(handles.buf_len,1);
handles.magdata = zeros(handles.buf_len,1);
handles.index = 1:handles.buf_len;

% Display x and y label ad title.
xlabel('Time');
ylabel('Magnitude');
title('Complete magnitude representation of the accelerometer sensor data');
    while strcmp(handles.str,'Stop Plot')
        
    
     %Updates str every time the while loop runs.
     %So when the button is pressed again, the while loop will break.
     handles.str = get(handles.doSomethingButton,'String');
     
     %read accelerometer output
     [handles.gx handles.gy handles.gz] = readAcc(handles.accelerometer, handles.calCo);
     
     
     %plot X and Y acceleration vectors and resultant acceleration vector
     
    %axes(handles.axes1);
    
    

    % Calculate the magnitude of the accelerometer axis readings
    %Students calculate the magnitude here
    handles.mag = sqrt(handles.gx*handles.gx + handles.gy*handles.gy...
        + handles.gz*handles.gz);
    % Append the new reading to the end of the rolling plot data. Drop the
    % first value
    handles.gxdata = [handles.gxdata(2:end) ; handles.gx];
    handles.gydata = [handles.gydata(2:end) ; handles.gy];
    handles.gzdata = [handles.gzdata(2:end) ; handles.gz];    
    handles.magdata = [handles.magdata(2:end) ; handles.mag];
    % Update the rolling plot
    
    %Students plot the magnitude here
    plot(handles.axes3,handles.index,handles.magdata,'g');
    
    axis(handles.axes3,[1 handles.buf_len -3.5 3.5]);  
    xlabel(handles.axes3,'time');
    ylabel(handles.axes3,'Magnitude of the resultant acceleration');
    
    % subplot for x y z magnitude
    
    plot(handles.axes4,handles.index,handles.gxdata,'r', handles.index,handles.gydata,'g', handles.index,handles.gzdata,'b');
    axis(handles.axes4,[1 handles.buf_len -3.5 3.5]);  
    xlabel(handles.axes4,'time');
    ylabel(handles.axes4,'Magnitude of individual axes acceleration');

    
     
     
     %Draws plot.
     drawnow;
    end
    cla(handles.axes3);
    cla(handles.axes4);
% Update handles structure
guidata(hObject, handles);


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
        

guidata(hObject, handles);


% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(handles.popupmenu2,'BackgroundColor','white');
end
handles.plotset = 0;
guidata(hObject, handles);




function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text

% collects value inputted to the edit box
f1 = str2double(get(handles.edit1,'String'));

% Separates values that the edit box can take based on whether the Normal
% Filter or SMA is active
if(strcmp(get(handles.text7,'String'),'Slider for Filter Value: 0 to 1'))
if( f1 >= 0 && f1 <= 1)
        set(handles.sliderfilt, 'Value',str2double(get(handles.edit1,'String')));
        set(handles.text4, 'String',get(handles.edit1,'String'));
        guidata(hObject, handles);
end
end


if(strcmp(get(handles.text7,'String'),'Slider for Filter Value: 0 to 100'))
    if( f1 > 1 && f1 <= 100)
        
        set(handles.sliderfilt, 'Value',str2double(get(handles.edit1,'String'))/100);
        set(handles.text4, 'String',get(handles.edit1,'String'));
        guidata(hObject, handles);
    end
end
    

guidata(hObject, handles);
        


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function sliderfilt_Callback(hObject, eventdata, handles)
% hObject    handle to sliderfilt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Determines the value of the pop up menu and uses a variable called
% plotset to adjust the slider settings accordingly

        handles.contents = cellstr(get(handles.popupmenu2,'String'));
        handles.a1 = handles.contents(get(handles.popupmenu2,'Value'));
        
        

        if(strcmp(handles.a1, 'SMA'))
    
        handles.plotset = 1;
        
        set(handles.text7, 'String','Slider for Filter Value: 0 to 100')
        
        set(hObject,'SliderStep',[.1,.01])
        handles.filtco = round(100*get(handles.sliderfilt,'Value'));
        
        end

        if(strcmp(handles.a1, 'Normal Filter'))
           
            handles.plotset = 0;
            set(handles.text7, 'String','Slider for Filter Value: 0 to 1');
            handles.filtco = get(handles.sliderfilt,'Value');
        end

set(handles.text4,'String',num2str(handles.filtco));

guidata(hObject, handles);
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderfilt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderfilt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end






% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Stores text on button to a string.
handles.str = get(handles.pushbutton7,'String');


% If statement to determine what text is currently on button
if strcmp(handles.str,'Start Plot with Filtering')
    
    % This will start the plot, and switch the button background and text
   set(handles.pushbutton7,'String','Stop Plot with Filtering');
   set(handles.pushbutton7,'BackgroundColor','red');
   set(handles.graphIndic,'BackgroundColor','cyan');
   set(handles.graphIndic,'String','Graphs are Active!');
   set(handles.doSomethingButton,'String','Start Plot'); 
   set(handles.doSomethingButton,'BackgroundColor','green');
  
   % Updates GUI
    guidata(hObject, handles);
    
else
    
   % This is if the plot is running. Stops plot and switches back to
   % original state
   set(handles.pushbutton7,'String','Start Plot with Filtering'); 
   set(handles.pushbutton7,'BackgroundColor','green');
   set(handles.graphIndic,'String','Graphs are Inactive');
   set(handles.graphIndic,'BackgroundColor','red');
   guidata(hObject, handles);
   
end

%Updates str to enter while loop with correct text
handles.str = get(handles.pushbutton7,'String');

% Sets up variables that will be used for filters
handles.buf_len = 200;
handles.gxdata = zeros(handles.buf_len,1);
handles.gydata = zeros(handles.buf_len,1);
handles.gzdata = zeros(handles.buf_len,1);
handles.magdata = zeros(handles.buf_len,1);
handles.index = 1:handles.buf_len;
handles.gxFilt = 0;
handles.gyFilt = 0;
handles.gzFilt = 0;
handles.u = 1;
handles.sumx = 0;
handles.sumy = 0;
handles.sumz = 0;
handles.gxTab = zeros(1,100);
handles.gyTab = zeros(1,100);
handles.gzTab = zeros(1,100);
handles.gxdata1 = zeros(handles.buf_len,1);
handles.gydata1 = zeros(handles.buf_len,1);
handles.gzdata1 = zeros(handles.buf_len,1);
handles.magdata1 = zeros(handles.buf_len,1);
set(handles.sliderfilt, 'Value', .5);
set(handles.text4,'String',num2str(get(handles.sliderfilt,'Value')));

% Display x and y label ad title.
xlabel('Time');
ylabel('Magnitude');
title('Complete magnitude representation of the accelerometer sensor data');
    while strcmp(handles.str,'Stop Plot with Filtering')
        
        % Determine value of pop up menu, must be updated every time loop
        % runs so that plot changing is instantaneous
        handles.contents = cellstr(get(handles.popupmenu2,'String'));
        handles.a1 = handles.contents(get(handles.popupmenu2,'Value'));
        
        

        if(strcmp(handles.a1, 'SMA'))
    
        handles.plotset = 1;
        end

        if(strcmp(handles.a1, 'Normal Filter'))
    
    
            handles.plotset = 0;
        end
        
     handles.filtco = get(handles.sliderfilt,'Value');
    
     %Updates str every time the while loop runs.
     %So when the button is pressed again, the while loop will break.
     handles.str = get(handles.pushbutton7,'String');
     
     %read accelerometer output
     [handles.gx handles.gy handles.gz] = readAcc(handles.accelerometer, handles.calCo);
     
     % If plotset is 0, Normal Filter is active, and these equations take
     % the slider value as the alpha value for each direction
     if handles.plotset == 0
     handles.gxFilt = (1-handles.filtco)*handles.gxFilt + handles.filtco*handles.gx;
     handles.gyFilt = (1-handles.filtco)*handles.gyFilt + handles.filtco*handles.gy;
     handles.gzFilt = (1-handles.filtco)*handles.gzFilt + handles.filtco*handles.gz;
     
     end
     cla;            %clear everything from the current axis
     
     
     % If plotset is 1, SMA is active and filtco is now 0-100
     if ( handles.plotset == 1)
         handles.filtco = round(100*get(handles.sliderfilt,'Value'));
         
         %Store the accelerometer value to the u spot in each matrix
         handles.gxTab(handles.u) = handles.gx;
         handles.gyTab(handles.u) = handles.gy;
         handles.gzTab(handles.u) = handles.gz;
         
         %Increment u
         handles.u = handles.u+1;
     
     % If u exceeds the SMA determination, produce the sum of the previous
     % filtco elements using for statement
     if handles.u > handles.filtco
         if( handles.filtco > 0)
         for i = 1:1:handles.filtco
             handles.sumx = handles.sumx + handles.gxTab(handles.u-i);
             handles.sumy = handles.sumy + handles.gyTab(handles.u-i);
             handles.sumz = handles.sumz + handles.gzTab(handles.u-i);
             
             %essentially a for loop that assigns the value of the previous
             %matrix value to the next matric value. It will not work for
             %the final value, which is omitted
             if i < handles.filtco
             handles.gxTab(i) = handles.gxTab(i+1);
             handles.gyTab(i) = handles.gyTab(i+1);
             handles.gzTab(i) = handles.gzTab(i+1);
             end
         end
         
         %Updates the Filters and resets the sums for reuse. In addition,
         %resets the u to filtco so that it will run every one step in the
         %future while still maintaining the correct SMA
         
         handles.sumx = handles.sumx / handles.filtco;
         handles.sumy = handles.sumy / handles.filtco;
         handles.sumz = handles.sumz / handles.filtco;
         handles.gxFilt = handles.sumx;
         handles.gyFilt = handles.sumy;
         handles.gzFilt = handles.sumz;
         handles.sumx = 0;
         handles.sumy = 0;
         handles.sumz = 0;
         handles.u = handles.filtco;
         end
     end
     end
     
     %plot X and Y acceleration vectors and resultant acceleration vector
     
    %axes(handles.axes1);
    
    

    % Calculate the magnitude of the accelerometer axis readings
    %Students calculate the magnitude here
    handles.magFilt = sqrt(handles.gxFilt*handles.gxFilt...
        + handles.gyFilt*handles.gyFilt + handles.gzFilt*handles.gzFilt);
    % Append the new reading to the end of the rolling plot data. Drop the
    % first value
    handles.gxdata = [handles.gxdata(2:end) ; handles.gxFilt];
    handles.gydata = [handles.gydata(2:end) ; handles.gyFilt];
    handles.gzdata = [handles.gzdata(2:end) ; handles.gzFilt];    
    handles.magdata = [handles.magdata(2:end) ; handles.magFilt];
    % Update the rolling plot
    
    %Students plot the magnitude here
    plot(handles.axes1,handles.index,handles.magdata,'g');
    
    axis(handles.axes1,[1 handles.buf_len -3.5 3.5]);  
    xlabel(handles.axes1,'time');
    ylabel(handles.axes1,'Magnitude of the resultant acceleration');
    
    % subplot for x y z magnitude
    
    plot(handles.axes2,handles.index,handles.gxdata,'r', handles.index,handles.gydata,'g', handles.index,handles.gzdata,'b');
    axis(handles.axes2,[1 handles.buf_len -3.5 3.5]);  
    xlabel(handles.axes2,'time');
    ylabel(handles.axes2,'Magnitude of individual axes acceleration');

    
     %%%%%%%%
     
    handles.mag = sqrt(handles.gx*handles.gx + handles.gy*handles.gy...
        + handles.gz*handles.gz);
    % Append the new reading to the end of the rolling plot data. Drop the
    % first value
    handles.gxdata1 = [handles.gxdata1(2:end) ; handles.gx];
    handles.gydata1 = [handles.gydata1(2:end) ; handles.gy];
    handles.gzdata1 = [handles.gzdata1(2:end) ; handles.gz];    
    handles.magdata1 = [handles.magdata1(2:end) ; handles.mag];
    % Update the rolling plot
    
    %Students plot the magnitude here
    plot(handles.axes3,handles.index,handles.magdata1,'g');
    
    axis(handles.axes3,[1 handles.buf_len -3.5 3.5]);  
    xlabel(handles.axes3,'time');
    ylabel(handles.axes3,'Magnitude of the resultant acceleration');
    
    % subplot for x y z magnitude
    
    plot(handles.axes4,handles.index,handles.gxdata1,'r', handles.index,handles.gydata1,'g', handles.index,handles.gzdata1,'b');
    axis(handles.axes4,[1 handles.buf_len -3.5 3.5]);  
    xlabel(handles.axes4,'time');
    ylabel(handles.axes4,'Magnitude of individual axes acceleration');

     
     %Draws plot.
     drawnow;
    end
    
    %Clear the axes
    cla(handles.axes1);
    cla(handles.axes2);
    cla(handles.axes3);
    cla(handles.axes4);
% Update handles structure
guidata(hObject, handles);
